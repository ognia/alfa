* [git](https://gitlab.com/ognia/alfa/wikis/git)
* [docker](https://gitlab.com/ognia/alfa/wikis/docker)
* [marathon](https://gitlab.com/ognia/alfa/wikis/marathon)
* [settings](https://gitlab.com/ognia/alfa/wikis/settings)